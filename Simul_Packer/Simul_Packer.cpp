// Simul_Packer.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include "../Common/common.h"

namespace user
{

#define RVATOVA(_base_, _offset_) ((PUCHAR)(_base_) + (ULONG)(_offset_))
	static void mark_pe_packed(PVOID Image)
	{
		__try
		{
			PIMAGE_NT_HEADERS32 pHeaders32 = (PIMAGE_NT_HEADERS32)
				((PUCHAR)Image + ((PIMAGE_DOS_HEADER)Image)->e_lfanew);

			auto _is64 = false;
			auto image_base = reinterpret_cast<char *>(Image);

			if (pHeaders32->FileHeader.Machine == IMAGE_FILE_MACHINE_I386)
			{
				// 32-bit image
				//if (pHeaders32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress)
				{
					DWORD old = 0;
					VirtualProtectEx(GetCurrentProcess(), pHeaders32, sizeof(IMAGE_NT_HEADERS32), PAGE_EXECUTE_READWRITE, &old);
					pHeaders32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress = 1;
					pHeaders32->OptionalHeader.AddressOfEntryPoint = 0;
				}
			}
			else if (pHeaders32->FileHeader.Machine == IMAGE_FILE_MACHINE_AMD64)
			{
				// 64-bit image
				PIMAGE_NT_HEADERS64 pHeaders64 = (PIMAGE_NT_HEADERS64)
					((PUCHAR)Image + ((PIMAGE_DOS_HEADER)Image)->e_lfanew);

				//if (pHeaders64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress)
				{
					DWORD old = 0;
					VirtualProtectEx(GetCurrentProcess(), pHeaders64, sizeof(IMAGE_NT_HEADERS64), PAGE_EXECUTE_READWRITE, &old);
					pHeaders64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress = 1;
					pHeaders64->OptionalHeader.AddressOfEntryPoint = 0;
					_is64 = true;
				}
			}
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			MessageBoxA(nullptr, "fucker", "ff", MB_OK);
		}


	}
	void mark_all_modules(PVOID Self)
	{
		NTDLL::PROCESS_BASIC_INFORMATION stInfo = { 0 };
		DWORD dwRetnLen = 0;
		DWORD dw = NTDLL::NtQueryInformationProcess(GetCurrentProcess(), NTDLL::ProcessBasicInformation, &stInfo, sizeof(stInfo), &dwRetnLen);
		PPEB pPeb = (PPEB)stInfo.PebBaseAddress;
		PLIST_ENTRY ListHead, Current;
		NTDLL::LDR_DATA_TABLE_ENTRY *pstEntry = NULL;

		ListHead = &(((PPEB)stInfo.PebBaseAddress)->Ldr->InMemoryOrderModuleList);
		Current = ListHead->Flink;
		while (Current != ListHead)
		{
			pstEntry = CONTAINING_RECORD(Current, NTDLL::LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks);
			mark_pe_packed(pstEntry->DllBase);
			Current = pstEntry->InMemoryOrderLinks.Flink;
		}
	}
};

int main()
{
	user::mark_all_modules(GetModuleHandle(nullptr));
	MessageBoxA(nullptr, "test", "me", MB_OK);
    return 0;
}

